core = 7.x
api = 2
projects[drupal][version] = "7.43"


; Core hacks :(

; Race condition in node_save() when not using DB for cache_field
; https://www.drupal.org/node/1679344#comment-10453619
projects[drupal][patch][] = "https://www.drupal.org/files/issues/drupal-7.36-transactional-cache-1679344-59.patch"

; Forums performance improvements
; https://www.drupal.org/node/145353#comment-8008797
projects[drupal][patch][] = "https://www.drupal.org/files/145353.diff"

; User's contact setting reverts to default when saving
; https://www.drupal.org/node/2451449#comment-9759823
projects[drupal][patch][] = "https://www.drupal.org/files/issues/2451449_4.diff"

; Canceling a user account, assigning content to Anonymous, should clear the comment name
; https://www.drupal.org/node/2472043#comment-9849229
projects[drupal][patch][] = "https://www.drupal.org/files/issues/canceling_a_user-2472043-17.patch"

; SchemaCache is prone to persistent cache corruption
; https://www.drupal.org/node/2450533#comment-11000623
projects[drupal][patch][] = "https://www.drupal.org/files/issues/2450533.diff"


; Modules

projects[advagg][version] = "2.18"

projects[apachesolr][version] = "1.8"

projects[apachesolr_multisitesearch][version] = "1.1"

projects[beanstalkd][version] = "1.x-dev"
; claimItem() "$lease_time" argument is incorrect
; https://www.drupal.org/node/2496115#comment-10139662
projects[beanstalkd][download][revision] = "716344f"

projects[block_class][version] = "2.3"

projects[bueditor][version] = "1.8"

projects[cache_consistent][version] = "1.2"

projects[codefilter][version] = "1.x-dev"
; <code><em>HTML in code</em></code> is not properly escaped with Prism
; https://www.drupal.org/node/2694791
projects[codefilter][download][revision] = "c0c7ea2"

projects[comment_fragment][version] = "1.1"

projects[composer_manager][version] = "1.7"

projects[conflict][version] = "1.0"

projects[ctools][version] = "1.x-dev"
projects[ctools][download][revision] = "a48f154"

projects[d3][version] = "1.x-dev"
projects[d3][download][revision] = "56b94df"
projects[d3_sparkline][version] = "1.x-dev"
projects[d3_sparkline][download][revision] = "2300b32"

projects[dereference_list][version] = "1.x-dev"
projects[dereference_list][download][revision] = "4acf25d"

projects[diff][version] = "3.2"

projects[distil_registration][version] = "1.x-dev"
projects[distil_registration][download][revision] = "5b6f623"
; Add a killswitch for empty UID blocking
; https://www.drupal.org/node/2710883#comment-11175573
projects[distil_registration][patch][] = "https://www.drupal.org/files/issues/2710883_0.diff"

projects[entity][version] = "1.6"

projects[entity_modified][version] = "1.2"
projects[entityreference][version] = "1.1"

projects[entityreference_prepopulate][version] = "1.6"

projects[extended_file_field][version] = "1.x-dev"
projects[extended_file_field][download][revision] = "b1ca3cf"

projects[facetapi][version] = "1.5"

projects[fasttoggle][version] = "1.x-dev"
; Fasttoggle doesn't support user relationships in views
; https://www.drupal.org/node/2598568
projects[fasttoggle][download][revision] = "98a9f0b"

projects[features][version] = "2.10"

projects[field_collection][version] = "1.x-dev"
projects[field_collection][download][revision] = "edb4e7d"

projects[field_extrawidgets][version] = "1.1"

projects[field_formatter_settings][version] = "1.1"

projects[field_group][version] = "1.5"

projects[filter_html_image_secure][version] = "1.1-beta1"

projects[flag][version] = "3.x-dev"
projects[flag][download][revision] = "357d41d"

projects[flag_tracker][version] = "2.0-rc1"

projects[flot][version] = "1.0"
projects[geolocation][version] = "1.4"

; Errors [notices] on 404 pages, waiting for 7.x-2.5 release.
; https://www.drupal.org/node/2126561
projects[google_admanager][version] = "2.x-dev"
projects[google_admanager][download][revision] = "d38afeb"

projects[google_analytics][version] = "2.1"

projects[homebox][version] = "2.0-rc2"

projects[honeypot][version] = "1.21"

projects[httpbl][version] = "1.0"

projects[jcarousel][version] = "2.7"

projects[libraries][version] = "2.2"

projects[link][version] = "1.4"

projects[logintoboggan][version] = "1.5"

projects[machine_name][version] = "1.x-dev"
projects[machine_name][download][revision] = "94b7446"

projects[mailchimp][version] = "3.3"

projects[memcache][version] = "1.3-rc1"

projects[message][version] = "1.11"

projects[message_notify][version] = "2.5"

projects[message_subscribe][version] = "1.x-dev"
; By default, don't notify blocked users
; https://www.drupal.org/node/2184567
projects[message_subscribe][download][revision] = "5feb3d5"

projects[message_follow][version] = "1.0-rc1"

projects[metatag][version] = "1.x-dev"
projects[metatag][download][revision] = "ede93e3"

projects[mollom][version] = "2.15"
; Improve $access_permissions array handling in _mollom_actions_access_callbacks()
; https://www.drupal.org/node/2680597#comment-10926165
projects[mollom][patch][] = "https://www.drupal.org/files/issues/2680597.diff"
; "Report node to Mollom as spam and unpublish" action should properly unpublish
; https://www.drupal.org/node/2590101#comment-10926237
projects[mollom][patch][] = "https://www.drupal.org/files/issues/2590101_1.diff"

projects[multiple_email][version] = "1.0-beta5"

projects[nodechanges][version] = "1.0-rc2"

projects[og][version] = "2.9"
; og_ui should give users the theme, not admin ui when creating groups
; https://www.drupal.org/node/1800208#comment-6545144
projects[og][patch][] = "http://drupal.org/files/og_ui-group_node_add_theme-1800208-5.patch"

projects[og_menu][version] = "3.x-dev"
; Add index to og_menu table
; https://www.drupal.org/node/2648462#comment-10743202
projects[og_menu][download][revision] = "35d5825"

projects[og_theme][version] = "2.0"

projects[panelizer][version] = "3.x-dev"
projects[panelizer][download][revision] = "fa9b8617f72a9fde277b456393a0ead6a7862715"
; Better Revision Handling
; http://drupal.org/node/2457113
projects[panelizer][patch][2457113] = "https://www.drupal.org/files/issues/panelizer-better_revision-2457113-48.patch"
; Basic translation (clone) support for panelizer pages
; https://www.drupal.org/node/2070891#comment-10579924
projects[panelizer][patch][2070891] = "https://www.drupal.org/files/issues/panelizer-add-language-support-2070891-1-7.patch"
; Add an index on node revisions
; https://www.drupal.org/node/1637304#comment-10634872
projects[panelizer][patch][1637304] = "https://www.drupal.org/files/issues/1637304-panelizer_entity_index-4.patch"
; Allow IPE without 'use panels in place editing' permission
; https://www.drupal.org/node/2657870#comment-10877084
projects[panelizer][patch][2657870] = "https://www.drupal.org/files/issues/2657870_1.diff"

projects[panels][version] = "3.x-dev"
projects[panels][download][revision] = "e8623b704fb2585bbf77f31f06d4a98721556277"
; Fix IPE JS alert (Panelizer is Incompatible with Moderation)
; http://drupal.org/node/1402860#comment-9729091
projects[panels][patch][1402860] = "https://www.drupal.org/files/issues/panelizer_is-1402860-82-fix-ipe-end-js-alert.patch"
; IPE Insufficient for working with Panelizer Revisioning
; https://www.drupal.org/node/2462331#comment-9778921
projects[panels][patch][2462331] = "https://www.drupal.org/files/issues/2462331-4.patch"
; Use display's context in panels_get_renderer()
; https://www.drupal.org/node/2663522#comment-10827214
projects[panels][patch][2663522] = "https://www.drupal.org/files/issues/2663522.diff"
; More-efficiently store *_allowed_types variables
; https://www.drupal.org/node/2671438#comment-10871240
projects[panels][patch][2671438] = "https://www.drupal.org/files/issues/2671438.diff"

projects[pathauto][version] = "1.2"

projects[project][version] = "2.x-dev"
projects[project][download][revision] = "9e0bf20"

projects[project_composer][version] = "1.x-dev"
projects[project_composer][download][revision] = "4d277b0"

projects[project_dependency][version] = "1.0-beta14"
; Store composer.json if present
; https://www.drupal.org/node/2551703#comment-11103421
projects[project_dependency][patch][] = "https://www.drupal.org/files/issues/parse_composer.json-2551703-34.patch"

projects[project_git_instructions][version] = "1.x-dev"
projects[project_git_instructions][download][revision] = "f9e7ac5"

projects[project_issue][version] = "2.x-dev"
projects[project_issue][download][revision] = "06b3d97"

projects[project_issue_file_test][version] = "3.x-dev"
projects[project_issue_file_test][download][revision] = "f3684dc"

projects[project_solr][version] = "1.x-dev"
projects[project_solr][download][revision] = "a9caaf4"

projects[r4032login][version] = "1.8"

projects[redirect][version] = "1.0-rc1"

; Fix and prevent circular redirects
; https://www.drupal.org/node/1796596#comment-8506117
projects[redirect][patch][] = "https://www.drupal.org/files/issues/redirect.circular-loops.1796596-146.patch"

projects[references][version] = "2.1"

projects[restws][version] = "2.4"

projects[role_activity][version] = "3.0-alpha2"

projects[sampler][version] = "1.0"

projects[search_api][version] = "1.18"

projects[search_api_db][version] = "1.5"
; Do not re-tokenize text
; https://www.drupal.org/node/2201041#comment-9174517
projects[search_api_db][patch][] = "https://www.drupal.org/files/issues/2201041-drupal.org-do-not-test.patch"

projects[signature_permissions][version] = "1.3"

projects[sshkey][version] = "2.0"

projects[strongarm][version] = "2.0"

projects[tfa][version] = "2.0"
projects[tfa_basic][version] = "1.0-beta2"

projects[token][version] = "1.5"
projects[token_formatters][version] = "1.2"
projects[url][version] = "1.0"
projects[user_restrictions][version] = "1.0"
projects[varnish][version] = "1.0-beta2"

; Dev needed for https://www.drupal.org/node/2680581
projects[versioncontrol][version] = "1.x-dev"
projects[versioncontrol][download][revision] = "50ba4b4"

projects[versioncontrol_git][version] = "1.x-dev"
projects[versioncontrol_git][download][revision] = "a280f89"

projects[versioncontrol_project][version] = "1.x-dev"
projects[versioncontrol_project][download][revision] = "a3f7085"

projects[views][version] = "3.11"

projects[views_bulk_operations][version] = "3.x-dev"
; No watchdog [option for] entry created on bulk node deletion
; https://www.drupal.org/node/2282079#comment-10614950
projects[views_bulk_operations][download][revision] = "f65fb0f"
; Multiple VBO Views on one page always use settings for the first VBO field
; https://www.drupal.org/node/2551301#comment-10485672
projects[views_bulk_operations][patch][] = "https://www.drupal.org/files/issues/2551301.diff"
; Default action behaviors in getAccessMask()
; https://www.drupal.org/node/2254871#comment-10464355
projects[views_bulk_operations][patch][] = "https://www.drupal.org/files/issues/2254871.diff"

projects[views_content_cache][version] = "3.0-alpha3"

projects[views_field_view][version] = "1.1"

projects[views_litepager][version] = "3.0"

projects[waiting_queue][version] = "1.2"


; Custom modules
projects[drupalorg][version] = "3.x-dev"
projects[drupalorg][download][revision] = "2f2e233"


; Libraries

libraries[d3][destination] = "libraries"
libraries[d3][directory_name] = "d3"
libraries[d3][download][type] = "get"
libraries[d3][download][url] = "https://github.com/mbostock/d3/releases/download/v3.4.4/d3.zip"

libraries[pheanstalk][destination] = "libraries"
libraries[pheanstalk][directory_name] = "pheanstalk"
libraries[pheanstalk][download][type] = "get"
libraries[pheanstalk][download][url] = "https://github.com/pda/pheanstalk/archive/v2.1.1.tar.gz"

libraries[flot][destination] = "modules"
libraries[flot][directory_name] = "flot/flot"
libraries[flot][download][type] = "get"
libraries[flot][download][url] = "http://www.flotcharts.org/downloads/flot-0.7.zip"

libraries[mailchimp][destination] = "libraries"
libraries[mailchimp][directory_name] = "mailchimp"
libraries[mailchimp][download][type] = "get"
libraries[mailchimp][download][url] = "https://bitbucket.org/mailchimp/mailchimp-api-php/get/2.0.6.tar.bz2"

; Required by tfa_basic
libraries[qrcodejs][destination] = "modules"
libraries[qrcodejs][directory_name] = "tfa_basic/includes/qrcodejs"
libraries[qrcodejs][download][type] = "git"
libraries[qrcodejs][download][url] = "https://github.com/davidshimjs/qrcodejs.git"
libraries[qrcodejs][download][revision] = "a714bbe"

; Required by codefilter_prism
libraries[prism][type] = "libraries"
libraries[prism][download][type] = "get"
libraries[prism][download][url] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/get/080140217839.zip"
libraries[prism][download][subtree] = "drupalorg-infrastructure-drupal.org-sites-common-080140217839/prism"


; More theme
projects[omega][type] = "theme"
projects[omega][version] = "4.4"

projects[drupalorg_themes][type] = "theme"
projects[drupalorg_themes][download][type] = "git"
projects[drupalorg_themes][download][url] = "git@bitbucket.org:drupalorg-infrastructure/drupalorg_themes.git"
projects[drupalorg_themes][download][revision] = "bd3af17"

; Common for Drupal.org D7 sites.
includes[drupalorg_common] = "https://bitbucket.org/drupalorg-infrastructure/drupal.org-sites-common/raw/7.x/drupal.org-common.make"
