<?php
// Ignore healthchecks in NewRelic, they skew our APM quite heavily.
if (extension_loaded('newrelic')) {
  newrelic_ignore_transaction (TRUE);
  newrelic_ignore_apdex (TRUE);
}

// Register our shutdown function so that no other shutdown functions run before this one.
// This shutdown function calls exit(), immediately short-circuiting any other shutdown functions,
// such as those registered by the devel.module for statistics.
define('DRUPAL_ROOT', getcwd());
register_shutdown_function('status_shutdown');
function status_shutdown() {
  exit();
}

// Drupal bootstrap.
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Build up our list of errors.
$errors = array();

// Check that the main database is active.
$account = db_select('users','u')
  ->fields('u')
  ->condition('uid',1,'=')
  ->execute()
  ->fetchObject();

if (!$account->uid == 1) {
  $errors[] = 'Master database not responding.';
}

// Check that the slave database is active.
if (function_exists('db_query_slave')) {
  $result = db_query_slave('SELECT * FROM {users} WHERE uid = 1');
  $account = db_fetch_object($result);
  if (!$account->uid == 1) {
    $errors[] = 'Slave database not responding.';
  }
}

// Check that all memcache instances are running on this server.
if (variable_get('memcache_servers')) {
  $memcache_servers = variable_get('memcache_servers');
  $memcache_count = count(variable_get('memcache_servers'));
  $memcache_errors = 0;
  $clusters = array();
  foreach ($memcache_servers as $server => $cluster) {
    list($ip, $port) = explode(':', $server);
    if (!memcache_connect($ip, $port)) {
      $memcache_errors++;
    }
  }
  if ($memcache_errors >= $memcache_count) {
    $errors[] = 'Memcache is not available.';
  }
}

$solr = apachesolr_get_solr();
if (!$solr->ping()) {
  $errors[] = 'Could not connect to solr.';
}

// Check that the files directory is operating properly.
if ($test = tempnam(variable_get('file_directory_path', conf_path() .'/files'), 'status_check_')) {
  if (!unlink($test)) {
    $errors[] = 'Could not delete newly create files in the files directory.';
  }
}
else {
  $errors[] = 'Could not create temporary file in the files directory.';
}

// Print all errors.
if ($errors) {
  $errors[] = 'Errors on this server will cause it to be removed from the load balancer.';
  header('HTTP/1.1 500 Internal Server Error');
  print implode("<br />\n", $errors);
}
else {
  // Split up this message, to prevent the remote chance of monitoring software
  // reading the source code if mod_php fails and then matching the string.
  print 'CONGRATULATIONS' . ' 200';
}

// Exit immediately, note the shutdown function registered at the top of the file.
exit();